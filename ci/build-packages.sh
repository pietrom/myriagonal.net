#!/bin/bash
targetDir="target"

for prj in $(find . -name *.nuspec)
do
	mono nuget.exe pack "${prj}" -OutputDirectory ./target
done
