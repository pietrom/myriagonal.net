#!/bin/bash
targetDir="target"

for package in $(find ${target} -name *.nupkg)
do
     mono nuget.exe push "${package}" -Source https://api.nuget.org/v3/index.json
done
